Imperva Puppet
==============

Overview
--------

To facilitate repeatable, encapsulated, on demand demonstrations,
the Imperva Puppet Vagrant-based virtual environment deploys a
single Ubuntu 16.04 server, the puppet agent, and a simple module
stack.

Prerequisite Software
---------------------

[VirtualBox](https://www.virtualbox.org/wiki/Downloads) version 5.1.38 or later

[VirtualBox Extension Pack](https://www.virtualbox.org/wiki/Downloads) should match your version of VirtualBox

[Vagrant](https://vagrantup.com) 2.2.3 or later

An Imperva Agent installation bsx file

Getting Started
---------------

Once all prerequisite software has been installed, clone the 
[puppet repo](https://gitlab.com/imperva-community/public/integrations/puppet) to
a local directory, and set that repository as your shell's current working directory.

```bash
cd ~
git clone https://gitlab.com/imperva-community/public/integrations/puppet.git
cd puppet
```

Create a directory for the bsx file

```bash
mkdir src
cp <your bsx filename> src
```
Configure the properties for the environment.

```bash
vi production/data/common.yaml
```

Running a demo
--------------

```bash
vagrant up
```

Stopping VMs
------------

When you are finished with a given VM, you can either suspend that VM for
use later, or destroy the VM (and ALL files within it!) if you do not expect
to need the VM for some time, or would like a fresh copy.

To destroy a VM:

```bash
vagrant destroy
```

To suspend a VM:
```bash
vagrant suspend
```

What VMs are running?
---------------------

You can see what VMs are running in the current directory by issuing:

```bash
vagrant status
```

Or see all of the vagrant controlled VMs on your machine with:
```bash
vagrant global-status
```

Re-provisioning VMs
-------------------

Puppet can also be re-run on your VMs, to reconfigure / restore the
puppet managed resourses to the desired state:

```bash
vagrant provision
```

Connecting to a VM
------------------

You can get an ssh shell (as the user vagrant) on any VM with:

```bash
vagrant ssh
```

You can also apply an ssh configuration which will allow you to use 
normal ssh commands with the VMs.

```bash
vagrant ssh-config
```

Will produce an ssh configuration file you can include in your ~/.ssh/config
file to skip the 'vagrant' portion of the command.

Managing the VM
---------------

You can do anything inside the VM that you would like, and aside from
the files in the host-side filesystems (mounted by default at /vagrant),
nothing you do in the VM will persist outside the VM itself.

The 'vagrant' user has password-less sudo inside the VM itself, and a root
shell in the VM can be obtained with:

```bash
vagrant ssh
sudo su -
```
