ToDo:

- create systemd unit file for ragent
- add conditional logic for multiple OS families
- extract debs and rpms from install shell script, and use package resource to manage them
- extract installer shell logic, and port to module
  - particularly when parsing kernel versions
- real unit and integration tests
- real README.md
- templates for ragent configuration
- custom facts for ragent
- module deps / metadata
