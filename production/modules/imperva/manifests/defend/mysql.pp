# Installs Imperva mysql agent
#
# @summary For demo purposes
#
# @example
#   include imperva::defend::mysql
class imperva::defend::mysql (
  String $agent_bsx       = 'Imperva-ragent-UBN-px86_64-b13.3.0.20.0.558523.bsx',
  String $dcfg            = '/usr/imperva/ragent/etc',
  String $dtarget         = '/usr/imperva/ragent/etc',
  String $dlog            = '/usr/imperva/ragent/etc/logs/cli',
  String $site            = "ESXi Lab",
  String $server_group    = 'SuperVeda DB',
  String $gwip            = '192.168.12.206',
  String $gwport          = '443',
  String $password        = 'Webco123#',
  Boolean $register_agent = false,
  Boolean $start_agent    = false,

){
  include apt

  exec { 'add_i386':
    command => '/usr/bin/dpkg --add-architecture i386',
    unless  => '/bin/grep -q i386 /var/lib/dpkg/arch',
    notify  => Exec['apt_update'],
  }
  package { 'libc6:i386':
    ensure => 'installed',
    require => [Exec['add_i386'],Exec['apt_update']],
  }
  ->exec { "echo 'Y' | /vagrant/src/${agent_bsx} -n -f -i":
      path => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/opt/puppetlabs/bin',
      creates => '/usr/imperva',
    }

  if $start_agent {
    $_notify_resource = Exec['/usr/imperva/ragent/bin/rainit start']
  } else {
    $_notify_resource = undef
  }

  if $register_agent {
    exec { 'register_imperva_agent':
      path    => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/opt/puppetlabs/bin',
      command => "/usr/imperva/ragent/bin/cli --dcfg ${dcfg} --dtarget ${dtarget} --dlog ${dlog} registration advanced-register registration-type=Primary is-db-agent=true tunnel-protocol=TCP ragent-name=Ubuntu_MySQL_${facts['hostname']} site=${site} server-group=${server_group} gw-ip=${gwip} gw-port=${gwport} manual-settings-activation=Automatic monitor-network-channels=Both password=${password}",
      notify => $_notify_resource,
    }
  }

  exec { '/usr/imperva/ragent/bin/rainit start':
    refreshonly => true,
  }
}
