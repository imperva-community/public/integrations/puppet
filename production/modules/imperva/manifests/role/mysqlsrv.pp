# Installs a mysql server, with optional Imperva agent
#
# @summary A demostration enviornment for Imperva agent
#
# @example
#   include imperva::role::mysqlsrv
class imperva::role::mysqlsrv (
  Boolean $install_agent = false
){
  include mysql::server

  if $install_agent {
    include imperva::defend::mysql
  }
}
